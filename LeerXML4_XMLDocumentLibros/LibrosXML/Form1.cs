﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LibrosXML
{



    public partial class Form1 : Form
    {
        //creación del documento xml vacío
        private XmlDocument docxml = new XmlDocument();

        public Form1()
        {
            InitializeComponent();
            this.docxml.Load("libros.xml");
            rellenarListBoxLibros();
        }


        //-----------------------------------------------------------------
        //                          MÉTODOS 
        //-----------------------------------------------------------------


        


        //-----------------------------------------------------------------
        //                      MÉTODOS AUXILIARES 
        //-----------------------------------------------------------------


        //Nos rellena el listBox con los titulos de los libros al leer desde su 'xml'
        private void rellenarListBoxLibros()
        {
            XmlNodeList libros = this.docxml.SelectNodes("//biblioteca/libro/titulo");

            foreach (XmlElement item in libros)
                listBoxTituloLibros.Items.Add(item.InnerText);
        }



        //Nos borra todo el contenido de los TextBox
        private void borrarTextBox()
        {
            tbGenero.Text = "";
            tbFechaPublicacion.Text = "";
            tbISBN.Text = "";
            tbTitulo.Text = "";
            tbAutor.Text = "";
            tbApellidosAutor.Text = "";
            tbPrecio.Text = "";
        }

        private void listBoxTituloLibros_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlNode nodo = this.docxml.SelectSingleNode("//biblioteca/libro[titulo='" + listBoxTituloLibros.SelectedItem.ToString() + "']");

            if (nodo != null)
            {
                tbGenero.Text = nodo.Attributes["genero"].InnerText;
                tbFechaPublicacion.Text = nodo.Attributes["fechadepublicacion"].InnerText;
                tbISBN.Text = nodo.Attributes["ISBN"].InnerText;
                tbTitulo.Text = listBoxTituloLibros.SelectedItem.ToString();
                tbAutor.Text = nodo.SelectSingleNode("autor/nombre").InnerText;
                tbApellidosAutor.Text = nodo.SelectSingleNode("autor/apellido").InnerText;
                tbPrecio.Text = nodo.SelectSingleNode("precio").InnerText;
            }
        }
    }

}
